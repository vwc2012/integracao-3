import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BookServiceService } from 'src/app/services/book/book-service.service';


@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.page.html',
  styleUrls: ['./add-book.page.scss'],
})
export class AddBookPage implements OnInit {

  bookForm: FormGroup;

  constructor(public formbuilder: FormBuilder,
              public bookService: BookServiceService) {
    this.bookForm = this.formbuilder.group({
      name: [null, [Validators.required]],
      author: [null, [Validators.required]],
      price: [null, [Validators.required]],
      summary: [null, [Validators.required]],
      condition: [null, [Validators.required]],
    })
  }

  createBook() {
    console.log(this.bookForm);
    let form = this.bookForm.value;
    console.log(form);
    this.bookService.createBook(form).subscribe(
      (res) => {
        console.log(res);
      }
    )
  }

  ngOnInit() {
  }

}
