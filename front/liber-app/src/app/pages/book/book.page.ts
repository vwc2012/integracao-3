import { Component, OnInit } from '@angular/core';
import { CommentService } from 'src/app/services/comment.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BookServiceService } from 'src/app/services/book/book-service.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.page.html',
  styleUrls: ['./book.page.scss'],
})
export class BookPage implements OnInit {

  commentForm: FormGroup;
  editCommentForm: FormGroup;
  book;
  comments;
  editMode = false;
  book_id = localStorage.getItem('book_id');
  comment_id: number;
  comment_text: any;

  constructor(public commentService: CommentService,
              public bookService: BookServiceService,
              public formbuilder: FormBuilder,) {
    this.commentForm = this.formbuilder.group({
      text: [null, [Validators.required, Validators.maxLength(140)]],
    })
    this.editCommentForm = this.formbuilder.group({
      text: [null, [Validators.required, Validators.maxLength(140)]],
    })
  }

  ngOnInit() {
    this.showBook();
    this.listComments();
  }

  showBook(){
    this.bookService.showBook(this.book_id).subscribe(
      (res) => {
        this.book = res.book;
        console.log(this.book);
      },
      (err) => {
        console.log(err);
      }
    )
  }

  listComments() {
    this.commentService.listComments().subscribe(
      (res) => {
        this.comments = res.comments;
        console.log(this.comments);
      },
      (err) => {
        console.log(err);
      }
    )
  }

  sendComment() {
    console.log(this.commentForm);
    let form = this.commentForm.value;
    form.book_id = localStorage.getItem('book_id');
    console.log(form);
    this.commentService.createComment(form).subscribe(
      (res) => {
        console.log(res);
        this.commentForm.reset();
        this.listComments();
      },
      (err) => {
        console.log(err);
      }
    )
  }

  toggleEdit(id, text){
      this.comment_id = id;
      this.comment_text = text;
      this.editMode=true; 
    }

  updateComment(){
    console.log(this.editCommentForm.value);
    this.commentService.updateComment(this.comment_id, this.editCommentForm.value).subscribe(
      (res) => {
        console.log(res);
        this.commentForm.reset();
        this.editMode = false;
        this.listComments();
      },
      (err) => {
        console.log(err);
      }
    )
  }

  deleteComment(id){
    this.commentService.deleteComment(id).subscribe(
      (res) => {
        console.log(res);
        this.commentForm.reset();
        this.listComments();
      },
      (err) => {
        console.log(err);
      }
    )
  }

  

 

}
