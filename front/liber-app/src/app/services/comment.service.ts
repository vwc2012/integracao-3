import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  apiUrl:string = 'http://localhost:8000/api/';

  constructor(public http: HttpClient) { }

  httpHeaders: any = {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  }

  createComment(form): Observable<any>{
    return this.http.post(this.apiUrl + 'comments', form);
  }

  showComment(id): Observable<any>{
    return this.http.get(this.apiUrl + 'comments/' + id, this.httpHeaders);
  }

  listComments(): Observable<any>{
    return this.http.get(this.apiUrl + 'comments', this.httpHeaders);
  }

  updateComment(id, form): Observable<any>{
    return this.http.put(this.apiUrl + 'comments/' + id, form);
  }

  deleteComment(id): Observable<any>{
    return this.http.delete(this.apiUrl + 'comments/' + id, this.httpHeaders);
  }
}
